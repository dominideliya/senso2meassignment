# -*- coding: utf-8 -*-
import sqlite3 as sql
import json

#get base sensor information
def get_base(sensor):
    bsensor = (sensor['type'], sensor['location'], sensor['hbmsg'], sensor['almsg']);
    return bsensor

# save base sensor to db
def save_base_sensor(connection, base_sensor):
    sql = ''' INSERT INTO Sensors(type,location,hbmsg,almsg)
              VALUES(?,?,?,?) '''
    cur = connection.cursor()
    cur.execute(sql, base_sensor)
    return cur.lastrowid

# save audio sensor data to db
def save_audio_sensor(connection, audio_sensor):
    #base table
    v_base_sensor = get_base(audio_sensor)
    id = save_base_sensor(connection, v_base_sensor)

    #audio table
    v_audio_sensor = (id, audio_sensor['noise'])
    sql = ''' INSERT INTO AudioSensor(recordid,noise)
              VALUES(?,?) '''
    cur = connection.cursor()
    cur.execute(sql, v_audio_sensor)
    connection.commit()


# save motion sensor data to db
def save_motion_sensor(connection, motion_sensor):
    #base table
    v_base_sensor = get_base(motion_sensor)
    id = save_base_sensor(connection, v_base_sensor)

    #motion table
    v_motion_sensor = (id, motion_sensor['movement'],motion_sensor['temperature'])
    sql = ''' INSERT INTO MotionSensor(recordid,movement,temperature)
              VALUES(?,?,?) '''
    cur = connection.cursor()
    cur.execute(sql, v_motion_sensor)
    connection.commit()


# save onbodysensor sensor data to db
def save_onbody_sensor(connection, onbody_sensor):
    #base table
    v_base_sensor = get_base(onbody_sensor)
    id = save_base_sensor(connection, v_base_sensor)

    #onbody table
    v_onbody_sensor = (id, onbody_sensor['accelaration'], onbody_sensor['temperature'])
    sql = ''' INSERT INTO OnbodySensor(recordid,accelaration,temperature)
              VALUES(?,?,?) '''
    cur = connection.cursor()
    cur.execute(sql, v_onbody_sensor)
    connection.commit()

# insert API

def save_data(json_data):
    #create connection
    con = sql.connect("..\\db\\sensor_database.db")
    # get the type from data
    sensor_type = json_data['type']
    # call correct method to save data according to type
    if sensor_type == 'A':
        save_audio_sensor(con, json_data)
    elif sensor_type == 'M' : save_motion_sensor(con, json_data)
    elif sensor_type == 'O' : save_onbody_sensor(con, json_data)

    #close the connection
    con.close()

# get data from audio sensors using json file
audiofilename = "..\\testdata\\audiodata.json"
data_audio = open(audiofilename).read()
json_data_audio = json.loads(data_audio)
save_data(json_data_audio)

# get data from motion sensors using json file
motionfilename = "..\\testdata\\motiondata.json"
data_motion = open(motionfilename).read()
json_data_motion = json.loads(data_motion)
save_data(json_data_motion)

# get data from onbody sensors using json file
onbodyfilename = "..\\testdata\\onbodydata.json"
data_onbody = open(onbodyfilename).read()
json_data_onbody = json.loads(data_onbody)
save_data(json_data_onbody)






