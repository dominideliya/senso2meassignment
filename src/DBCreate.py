import sqlite3 as sql

# read SQL file
sqlcontent = open('..\\sql\\sensor_data_tables.sql', 'r').read()
sql.complete_statement(sqlcontent)

#connecting to the database file
conn = sql.connect("..\\db\\sensor_database.db")
cursor = conn.cursor()

try:
    cursor.executescript(sqlcontent)
except Exception as e:
    print(str(e))
    cursor.close()
    raise