# Project description

## Intro

Senso2Me designs multiple types of sensors. All sensors have the same base
system, for both hardware ans firmware, but carry some different traits. For
instance, all sensors have an alert button that, when pressed, will let the
sensor send out a distress signal (alert message). Additionally, each sensors
sends heartbeat messages at certain intervals (for example, every 360 seconds).

Amongst the sensor types that Senso2Me designs are:

* Motion sensors that detect movement as a 1 bit value and measure temperature
  and light as 1 byte values
* Audio sensors that only measure ambient noise levels as a 2 byte value.
* On-body sensors that measure acceleration, temperature and light as 2 byte
  values.
The former 2 sensor types are placed in a care recipients's residence, while the
latter is carried by either care recipient or care taker.

## Assignment

* Create a database design that will hold information of sensors that are
  currently in use. The database design should take into account the location
  of the sensor and should also store heartbeat messages and alert messages.
* Create a simple API that can be used to insert an alert message into the
  database whenever a sensor button is pressed.

## Notes

* You may use any tools, languages or library you like, but take into account
  the stability and ecosystem (just like you would when you create a production
  application)
* Please document your every step/decision and provide some unit tests
* Provide your solution through a VCS (github/bitbucket/...)
