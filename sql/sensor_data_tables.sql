DROP TABLE if EXISTS Sensors;
    CREATE TABLE Sensors (
    recordid INTEGER PRIMARY KEY autoincrement,
    type text NOT NULL,
    location text NOT NULL,
    hbmsg text,
    almsg text
);

DROP TABLE if EXISTS MotionSensor;
CREATE TABLE MotionSensor(
    recordid INTEGER PRIMARY KEY autoincrement,
    movement BIT,
    temperature INTEGER,
    FOREIGN KEY (recordid) REFERENCES Sensors(recordid)
);

DROP TABLE if EXISTS AudioSensor;
CREATE TABLE AudioSensor(
   recordid INTEGER PRIMARY KEY autoincrement,
   noise SMALLINT	,
   FOREIGN KEY (recordid) REFERENCES Sensors(recordid)
);

DROP TABLE if EXISTS OnbodySensor;
CREATE TABLE OnbodySensor(
   recordid INTEGER PRIMARY KEY autoincrement,
   accelaration SMALLINT,
   temperature SMALLINT,
   FOREIGN KEY (recordid) REFERENCES Sensors(id)
);